module Mvideo
  class Parser
    AUTH = "&oauth_token=c883ee85fe574b8d8d5d2ca62fd9b7e6&oauth_client_id=ac448773268a4ee49e954b09c779f319&oauth_login=eders1978"
    def initialize(options)
      @stores = {}
      Store.all.each{ |s| @stores[s.code] = s.id }
      @domain = "http://" + options[:domain]
      @start_url = @domain
      @cookies = options[:cookies]
    end

    def perform
      p '============================= МВидео ========================================'

      
      Category.subcategories.each do |s|
        subcategory = {id: s.id, url: s.url.sub(@domain, ''), title: s.title}
        p "#{Time.now} Parsing goods data for #{subcategory[:title]}..."
        @goods, @qtys = [], []
        get_goods_info subcategory

        p "Saving goods...#{Time.now}"
        save_goods
      end    
      
      p "МВидео. Parsing done! #{Time.now}"
    end
    
    def parse_stores_categories
      p "Parsing stores... #{Time.now}"
      get_stores
      
      p "Parsing main categories... #{Time.now}"
      categories = get_categories
      
      p "Parsing subcategories... #{Time.now}"
      get_subcategories categories
    end
    
    def parse_tv
      Category.where(parent_id: 1).each do |s|
        @goods, @qtys = [], []
        subcategory = {id: s.id, url: s.url.sub(@domain, '')}
        ap subcategory
        get_goods_info subcategory

        p "Saving goods...#{Time.now}"
        save_goods
      end
      ap "Parse tv dove"
    end
    
    
          
    def features
      get_goods_fetures
    end
    
    def scan_by_yandex_model
      p "Start scan by yandex #{Time.now}"
      @goods = Good.not_found_in_yandex
      threads = []
      @goods.each_slice((@goods.count / 2) + 1) do |goods|
#        threads << Thread.new do
          goods.each do |good|
            resp = get_models_from_yandex(good.title) || get_models_from_yandex(good.title.gsub(/\p{Cyrillic}/, '').strip) || get_models_from_yandex(good.short_name)    
            if resp
              good.is_yandex_model = true
              good.yandex_id = resp['models'].first['id'].to_s.to_i
              ap "Query: #{@query}"
              ap good.yandex_id 
              ap good.id
            else
#              good.is_yandex_model = false
#              good.yandex_id = nil
            end 
            good.save
          end
#        end  
      end  
#      threads.each(&:join)
      p "Done scan by yandex (found: #{Good.found_in_yandex.count} from: #{Good.count}) #{Time.now}"
    end
    
    
    def save_csv
      p "CSV goods start #{Time.now}"
      @goods = Good.all
      CSV.open("/var/www/ror/grabber_app/public/csv/mvideo_parse.csv", "wb") do |csv|
        csv << ["Категория", "Бренд", "Наименование", "Короткое наименование", "URL", "Ключ мвидео", "Цена", "Картинка", "YandexId"]
        @goods.each do |good|
          csv << [good.category.title, good.brend, good.title, good.short_name, good.url, good.foreign_id, good.price, good.image, good.yandex_id]
        end
      end
      p "CSV goods saved #{Time.now}"
      p "CSV features start #{Time.now}"
      @good_features = GoodFeature.includes(:good)
      CSV.open("/var/www/ror/grabber_app/public/csv/mvideo_goods_features.csv", "wb") do |csv|
        csv << ["id товара", "Ключ мвидео", "Тип характеристики", "Характеристика", "Значение"]
        @good_features.each do |feature|
          csv << [feature.good_id, feature.good.foreign_id, feature.title, feature.name, feature.sign]
        end
      end
      p "CSV features saved #{Time.now}"
    end
    
    def get_small_images
      p "Start loading small images #{Time.now}"
      @images = Good.all.pluck(:image)
      @images.each do |image|
        File.open("images/img_m/" + image[image.rindex('/')+1..-1], 'wb') do |file|
          file << open(image).read
        end
      end  
      p "Small images loaded #{Time.now}"
    end
    
    def get_big_images
      p "Start loading big images #{Time.now}"
      @images = Good.all.pluck(:image)
      @images.each do |image|
        File.open("images/img_b/" + image[image.rindex('/')+1..-1].sub('m', "b"), 'wb') do |file|
          file << open(image.sub('m.jpg', "b.jpg")).read
        end
      end  
      p "Big images loaded #{Time.now}"
    end
    
    
    def updating_prices
      p "Start updating prices... #{Time.now}"
      threads = []
      Good.all.each_slice(Good.all.size / 5) do |goods|
        threads << Thread.new do
          goods.each do |good|
            begin
              doc = Nokogiri::HTML(open(good.url, "Cookie" => @cookies), nil, 'utf-8')
              price = doc.css('.product-details-summary-price-holder .product-price-current').text.to_i
              good.update_attribute(:price, price)
            rescue => error
              p "Error updating prices...#{good.url} | #{Time.now}"
              p error.inspect
            end  
          end  
        end
      end  
      threads.each(&:join)
      p "Updating prices done #{Time.now}"
    end
    
    
    private

    def get_stores
      @stores = {}
      url = @domain + "/shops/store-list"
      doc = Nokogiri::HTML(open(url, "Cookie" => @cookies), nil, 'utf-8')
      doc.css('li.store-locator-list-item').each do |store|
        title = store.css('.name h3').text.force_encoding(Encoding::UTF_8).to_s.strip
        s = Store.create code: store["id"], title: title
        p store.css('.name h3').text.strip
        @stores[s.code] = s.id
      end
    end

    def get_categories
      doc = Nokogiri::HTML(open(@start_url, "Cookie" => @cookies))
      categories = doc.css('a.header-nav-item-link')
        .map{|cat| {title: cat.css("span").first.text, url: cat["href"]}}
        .reject{|cat| cat[:url] == "#"}
      categories.each_with_index do |category, index|
        categories[index][:id] = Category.create(title: category[:title]).id
      end
    end

    def get_subcategories(categories)
      subcategories = []
      threads = []
      categories.each do |category|
        threads << Thread.new do
          category_url = @domain + category[:url]
          doc = Nokogiri::HTML(open(category_url, "Cookie" => @cookies)) 
          subcategories << doc.css('li.sidebar-category a')
            .map{|cat| {title: cat.text, url: cat["href"], parent: category[:id]}}
        end
      end
      threads.each { |t| t.join }
      subcategories.flatten!.each_with_index do |category, index|
        subcategories[index][:id] = Category.create(title: category[:title], parent_id: category[:parent], url: @domain + category[:url]).id
      end
    end
    
    def get_subcategories_features
      threads = []
      #ap Category.subcategories
      Category.subcategories.each do |subcategory|
        
        #threads << Thread.new do
          begin
            doc = Nokogiri::HTML(open(subcategory.url, "Cookie" => @cookies)) 
            doc.css('.facet-heading').map { |f| f.text.to_s.strip }.each do |feature| 
              subcategory.category_features << CategoryFeature.new(name: feature)
            end
          rescue
            p "Error loading subcategories_features #{subcategory.url}"
          end  
        #end  
      end
      #threads.each(&:join)
    end
    
    def get_goods_fetures
      p "Parsing goods features... #{Time.now}"
      GoodFeature.delete_all
      p "Delete all #{Time.now}"
      @goods = Good.all
      threads = []
      @goods.each_slice(Good.all.size / 5) do |goods|
        threads << Thread.new do
          goods.each do |good|
            descr_url = good.url + '?ssb_block=descriptionTabContentBlock&ajax=true'
            begin    
              descr_doc = Nokogiri::HTML(open(descr_url, "Cookie" => @cookies), nil, 'utf-8')
              descr_doc_box = descr_doc.css('.product-details-tables-holder')
              descr_doc_titles = descr_doc_box.css('h3')
              descr_doc_tables = descr_doc_box.css('table')

              descr_doc_titles.map { |t| t.text }.each_with_index do |title, index|
                descr_doc_tables[index].css('tr').each do |tr|
                  good.good_features << GoodFeature.new(title: title, name: tr.css('td').first.text, sign: tr.css('td')[1].text) unless tr.css('td').blank?
                end  
              end
            rescue  => error
              p "Error loading goods_features #{descr_url}"
              p " #{error.inspect}"
            end  
          end
        end  
      end
      threads.each(&:join)
      p "Loading goods_features done #{Time.now}"
    end
    
    

    def get_goods_info(category, page = 1, pages = nil)
      p "#{Time.now}  page #{page}"
      page_url = @domain + category[:url]
      begin
        doc = Nokogiri::HTML(open(page_url, "Cookie" => @cookies))
        page_link = doc.css('.pagination ul li a.pagination-item-link').last['href'] if doc.css('.pagination ul li a.pagination-item-link').last
        pages_count = pages || doc.css('.pagination ul li a.pagination-item-link').last.try(:text).try(:to_i) || 1
        if page_link
          page_link = page_link.sub("page=#{pages_count}","page=#{page}") 
          doc = Nokogiri::HTML(open(@domain + page_link, "Cookie" => @cookies))
        end  
      rescue => error
        p "#{Time.now}  error open category"
        p error.inspect
      end  
#      ap page_url
#      ap page_link

      threads = []
      goods = doc.css('.product-tiles-list .product-tile')
      goods.each do |good|
        threads << Thread.new do
          begin
            title = good.css('.product-tile-title-link').first['title']
            foreign_id = good.css('.product-tile-title-link').first['href'][/\d+$/].to_i
            price = good.css('.product-price-current').text
            #small_image = "http:#{good.css('img.product-tile-picture-image').first['data-original']}"
            good_url = @domain + good.css('.product-tile-title-link').first['href']
            stores_url = good_url + '?ssb_block=availabilityContentBlockContainer&ajax=true'

            good_doc = Nokogiri::HTML(open(good_url, "Cookie" => @cookies), nil, 'utf-8')
            bonus = good_doc.css('.product-details-summary-bonus-info').text.to_s.strip.to_i
            brend = good_doc.css('.product-data-header img').first['alt'].sub('_', ' ') if good_doc.css('.product-data-header img').first
            image = good_doc.css('.image-holder img.product-image').first['src']
            short_name = title.gsub(/\p{Cyrillic}/, '').sub(brend || '+', '').sub('.', ' ').sub('-', ' ').sub('(', ' ').sub(')', ' ').strip
            @goods << {title: title, price: price, image: image, category_id: category[:id], foreign_id: foreign_id, 
                url: good_url, bonus: bonus, brend: brend , short_name: short_name}
          rescue => error
            p "#{Time.now} Error loading goods_info #{page_url}"
            p error.inspect
          end    

          begin
            s_doc = Nokogiri::HTML(open(stores_url, "Cookie" => @cookies), nil, 'utf-8')
            s_doc.css('li.store-locator-list-item').each do |store|
              s_code = store.css('.name h3 a').first["href"][/S\d+/]
              store_id = @stores[s_code]
              qty_ex = store.css('.stock i').first
              qty = qty_ex ? qty_ex["data-title"] : ''
              @qtys << {foreign_id: foreign_id, store_id: store_id, qty: qty}
            end
          rescue
            p "#{Time.now} Error loading stores #{stores_url}"
          end
        end
        threads.each(&:join)
      end
      p @goods.size
      
      get_goods_info(category, page + 1, pages_count) if pages_count > page
    end

    def save_goods
      @goods.uniq! { |good| good[:foreign_id]}
      foreigns = Good.pluck(:foreign_id)
      @goods.delete_if do |good|
        foreigns.include?(good[:foreign_id])
      end

      p "Saved #{@goods.size}"
      map = Good.create(@goods).group_by &:foreign_id
      map.each {|k, v| map[k] = v.first.id}
      #Good.dedupe
      @qtys.each_with_index do |q, index|
        @qtys[index][:good_id] = map[q[:foreign_id]]
        @qtys[index].delete(:foreign_id)
      end
      Quantity.create @qtys
      @goods = []
      @qtys = []

    end
    
    def get_models_from_yandex query
      @url = "https://api.partner.market.yandex.ru/v2/models.json?regionId=2&query=#{URI.encode query}" + AUTH
      @query = query
      uri = URI.parse(@url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      resp = response.body
      unless response.code == "200"
        File.open("log/models.log", 'a') do |file| 
          file.write response.body.inspect; 
          file.write Time.now; file.write "\n" 
        end  
        p response.body.inspect
      end
      resp = JSON.parse(resp)
      if resp && resp['pager'] && resp['pager']['total'] == 1
        resp
      else  
        false
      end
    rescue => error
      File.open("log/models.log", 'a') do |file| 
          #file.write error.inspect 
          file.write Time.now; file.write "\n" 
      end
      p error.inspect 
      nil
    end
    
  end
end

