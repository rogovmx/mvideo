class Category < ActiveRecord::Base
  validates :title, presence: true, uniqueness: true
  
  has_many :goods
  has_many :category_features
  
  scope  :subcategories, -> { where.not(parent_id: nil)}
  
end