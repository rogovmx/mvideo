class CategoryFeature < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  
  belongs_to :category
end