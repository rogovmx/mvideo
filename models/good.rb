class Good < ActiveRecord::Base
  self.primary_key = :id
  
  validates :title, presence: true
  validates :category_id, presence: true

  has_many :good_features
  has_many :quantities
  has_one :yandex_model
  belongs_to :category
  
  scope :found_in_yandex, -> {where(is_yandex_model: true)}
  scope :not_found_in_yandex, -> {where.not(is_yandex_model: true)}
  scope :no_features, -> {joins(:good_features).where("good_features.good_id IS NULL")}
  
    
  
  def self.dedupe
    grouped = all.group_by{|good| [good.foreign_id] }
    grouped.values.each do |duplicates|
      first_one = duplicates.shift
      duplicates.each{|double| double.destroy}
    end
  end
  
  
  
end