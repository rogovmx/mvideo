class GoodFeature < ActiveRecord::Base
  validates :name, :title, presence: true
  
  belongs_to :good
end