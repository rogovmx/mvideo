class CreateTables < ActiveRecord::Migration
  def change
    
    create_table :stores, force: true do |t|
      t.string :title
      t.string :code
      t.timestamps null: false
    end
    add_index :stores, :code


    create_table :quantities, force: true do |t|
      t.references :store
      t.references :good
      t.string :qty
      t.timestamps null: false
    end
    add_index :quantities, :good_id

    create_table :category_features, force: true do |t|
      t.references :category
      t.string :name
      t.timestamps null: false
    end
    add_index :category_features, :category_id

    create_table :good_features, force: true do |t|
      t.references :good
      t.string :title
      t.string :name
      t.string :sign
      t.timestamps null: false
    end
    add_index :good_features, :good_id

    create_table :categories, force: true do |t|
      t.string :title
      t.string :url
      t.integer :lft
      t.integer :rgt
      t.timestamps null: false
      t.references :parent, default: nil
    end

    create_table :goods, force: true do |t|
      t.integer :foreign_id
      t.string :title
      t.string :brend
      t.string :short_name
      t.string :image
      t.string :url
      t.integer :bonus
      t.decimal :price, precision: 12, scale: 2
      t.boolean :is_yandex_model, default: false
      t.string :yandex_id
      t.timestamps null: false
      t.references :category
    end

    
  end
end